
public class Main {

	public static void main(String[] args) {
		Library lib = new Library();
		
		HelloWorldWindow window = new HelloWorldWindow();
		window.initialize(lib.someLibraryMethod());
	}

}
